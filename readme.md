balanceHeights is a jQuery plugin for balancing heights of a group of elements

The height will be set on all childs of elements with the class `js-balanceHeights`.

The group of elements can be set individually by adding a data attribute to the acendent element, e.g.:

```html
<div class="js-balanceHeights" data-balanced-heights-descendants="> span">
```

You can also apply the plugin manually, e.g:

```javascript
$(".container").balanceHeights({
    descendants: ".group-of-elements"
});
```

The parameter `descendants` is optional.