/**
 * BalanceHeights is a jQuery plugin for balancing heights of a group of elements
 * https://gitlab.com/supermueller/balance-heights/
 *
 * - The height will be set on all childs of elements with the class `js-balanceHeights`.
 * - The group of elements can be set individually by adding a data attribute to
 *     the acendent element, e.g.:
 *     `<div class="js-balanceHeights" data-balanced-heights-descendants="> span">`
 */

$(function(){
		// Apply to elements with standard class `js-balanceHeights`
	$(".js-balanceHeights").balanceHeights();

});


(function ( $ ){

	var defaults = {
		descendants: "> *"
	};

	$.fn.balanceHeights = function(options){

		var options = options || {};

		return this.each(function(){

			var container = $(this);

				/* Settings defined by data attributes */

			var data = {
			}

			if (container.data("balanced-heights-descendants")){
				data.descendants = container.data("balanced-heights-descendants")
			}

				/* Combine settings */

			var settings = $.extend(true, {}, defaults, options, data);

			new Balancer(container, settings);

		});

	};

	var Balancer = function(container, settings){

		var self = this;

		this.container = container;
		this.settings = settings;

		if (typeof this.settings.descendants !== "undefined"){
			var descendants = $(this.settings.descendants, this.container);
			var heights = this.setHeights(descendants);
		}

		return;

	};

	Balancer.prototype.setHeights = function(elements){

		var max_height = 0,
			various_heights = false;

		elements.each(function(index){

			var height = $(this).outerHeight();

			if (index > 0 && ! various_heights){
				various_heights = (height !== max_height);
			}

			if (height > max_height){
				max_height = height;
			}

		});

		if (! various_heights){

			return {
				max: "auto",
				are_various: false
			};
		}

		elements.each(function(index){
			$(this).outerHeight(max_height);
		});

		return {
			max: max_height,
			are_various: various_heights
		};

	}

}( jQuery ));